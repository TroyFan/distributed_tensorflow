#!/bin/bash

LOGS=/tmp/train_logs

if [ -d "$LOGS" ]; then
	rm -rf $LOGS/*
fi

python lenet.py --ps_hosts=localhost:2222 --worker_hosts=localhost:2223 --data_dir=mnist --job_name=ps --task_index=0 &
python lenet.py --ps_hosts=localhost:2222 --worker_hosts=localhost:2223 --data_dir=mnist --job_name=worker --task_index=0 --step=1000 &
