import argparse
import sys
import time
import os

from tensorflow.examples.tutorials.mnist import input_data
import tensorflow as tf
import resnet as resnet

FLAGS = None

def create_queue(job_name, task_index, worker_hosts):
  with tf.device("/job:%s/task:%d" % (job_name, task_index)):
    return tf.FIFOQueue(len(worker_hosts), tf.int32, shared_name="queue_"+str(job_name)+"_"+str(task_index))

def main(_):
  ps_hosts = FLAGS.ps_hosts.split(",")
  worker_hosts = FLAGS.worker_hosts.split(",")

  # Create a cluster from the parameter server and worker hosts.
  cluster = tf.train.ClusterSpec({"ps": ps_hosts, "worker": worker_hosts})

  # Create and start a server for the local task.
  server = tf.train.Server(cluster,
                           job_name=FLAGS.job_name,
                           task_index=FLAGS.task_index)

  if FLAGS.job_name == "ps":

    # Control shutdown of parameter server in queue instead of server.join() function.
    queue = create_queue(FLAGS.job_name, FLAGS.task_index, worker_hosts)

    with tf.Session(server.target) as sess:
      for i in range(len(worker_hosts)):
        sess.run(queue.dequeue())

  elif FLAGS.job_name == "worker":

    # Assigns ops to the local worker by default.
    with tf.device(tf.train.replica_device_setter(
        worker_device="/job:worker/task:%d" % FLAGS.task_index,
        cluster=cluster)):

      # Import data
      mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

      # Build Deep MNIST model...
      global_step = tf.train.get_or_create_global_step()

      # ResNet
      model = resnet.ResNet(global_step=global_step, name="resnet", lr=1e-4, layer_n=3, SEED=777)
      train_step = model.train_op
      accuracy = model.accuracy
      
    steps = FLAGS.step

    # Create queues for all servers participating in the cluster.
    queue = create_queue(FLAGS.job_name, FLAGS.task_index, worker_hosts)
    queues = []
    for i in range(len(ps_hosts)):
      queues.append(create_queue("ps", i, worker_hosts))
    for i in range(len(worker_hosts)):
      queues.append(create_queue("worker", i, worker_hosts))

    # The StopAtStepHook handles stopping after running given steps.
    batch = mnist.validation.next_batch(100)
    val_feed = {model.X: batch[0], model.y: batch[1], model.training: False}
    final_acc = tf.train.FinalOpsHook(final_ops=accuracy, final_ops_feed_dict=val_feed)  
    hooks=[tf.train.StopAtStepHook(last_step=steps), final_acc]
    # The MonitoredTrainingSession takes care of session initialization,
    # restoring from a checkpoint, saving to a checkpoint, and closing when done
    # or an error occurs.
    with tf.train.MonitoredTrainingSession(master=server.target,
                                           is_chief=(FLAGS.task_index == 0),
                                           checkpoint_dir=FLAGS.log_dir,
                                           hooks=hooks) as mon_sess:
      i = 0
      while not mon_sess.should_stop():
        # Run a training step asynchronously.
        batch = mnist.train.next_batch(50)
        if i % 100 == 0:
          train_accuracy = mon_sess.run(accuracy, feed_dict={
              model.X: batch[0], model.y: batch[1], model.training: False})
          print('global_step %s, task:%d_step %d, training accuracy %g'
                % (tf.train.global_step(mon_sess, global_step), FLAGS.task_index, i, train_accuracy))

	if tf.train.global_step(mon_sess, global_step) == 0:
	  print('start_time')
	  start_time = time.time()
	  fo = open(FLAGS.log_dir + "/start", "w+")
	  fo.write('%f' % start_time)
	  fo.close()

	mon_sess.run(train_step, feed_dict={model.X: batch[0], model.y: batch[1], model.training: True})
	  
	i = i + 1

    #with tf.train.MonitoredTrainingSession(master=server.target,
    #                                       is_chief=(FLAGS.task_index == 0),
    #                                       checkpoint_dir=FLAGS.log_dir
    #                                       ) as mon_sess:
      # Validation
      # val_feed = {model.X: mnist.validation.images, model.y: mnist.validation.labels, model.training: False}  
      # val_xent = mon_sess.run(accuracy, feed_dict=val_feed)
      #fo = open(FLAGS.log_dir + "/accuracy", "w+")
      #fo.write('accuracy: %f' % val_xent)
      #fo.close()
    print('end_time')
    fo = open(FLAGS.log_dir + "/end", "w+")
    fs = open(FLAGS.log_dir + "/start")
    start_time = float(fs.read())
    end_time = time.time()
    elasped_time = end_time - start_time
    print('Training time: %f' % elasped_time)
    fo.write('%f' % elasped_time)
    fo.close()
    fs.close()
    
    print('accuracy: %f' %final_acc._final_ops_values)
    # Notification of task completion and wait for task completion of other worker server.
    with tf.Session(server.target) as sess:
      for q in queues:
        sess.run(q.enqueue(1))
      for i in range(len(worker_hosts)):
        sess.run(queue.dequeue())

if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.register("type", "bool", lambda v: v.lower() == "true")
  # Flags for defining the tf.train.ClusterSpec
  parser.add_argument(
      "--ps_hosts",
      type=str,
      default="",
      help="Comma-separated list of hostname:port pairs"
  )
  parser.add_argument(
      "--worker_hosts",
      type=str,
      default="",
      help="Comma-separated list of hostname:port pairs"
  )
  parser.add_argument(
      "--job_name",
      type=str,
      default="",
      help="One of 'ps', 'worker'"
  )
  # Flags for defining the tf.train.Server
  parser.add_argument(
      "--task_index",
      type=int,
      default=0,
      help="Index of task within the job"
  )
  # Flags for specifying input/output directories
  parser.add_argument(
      "--data_dir",
      type=str,
      default="/tmp/mnist_data",
      help="Directory for storing input data")
  parser.add_argument(
      "--log_dir",
      type=str,
      default="/tmp/train_logs",
      help="Directory for train logs")
  parser.add_argument(
      "--step",
      type=int,
      default=300,
      help="Training step")
  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
