#!/bin/bash

kill -9 `ps -aux | grep train_resnet | sed '$ d' | awk '{print $2}'`
