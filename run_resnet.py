#!/bin/bash

LOGS=/tmp/train_logs

if [ -d "$LOGS" ]; then
	rm -rf $LOGS/*
fi

python train_resnet.py --ps_hosts=localhost:2222 --worker_hosts=localhost:2223,localhost:2224 --data_dir=mnist --job_name=ps --task_index=0 &
python train_resnet.py --ps_hosts=localhost:2222 --worker_hosts=localhost:2223,localhost:2224 --data_dir=mnist --job_name=worker --task_index=0 --step=100 &
python train_resnet.py --ps_hosts=localhost:2222 --worker_hosts=localhost:2223,localhost:2224 --data_dir=mnist --job_name=worker --task_index=1 --step=100 &
