#!/bin/bash

LOGS=/tmp/train_logs

#if [ -d "$LOGS" ]; then
#	rm -rf $LOGS/*
#fi

#python lenet.py --ps_hosts=localhost:2222 --worker_hosts=node2:2222,node3:2222,node4:2222 --data_dir=mnist --job_name=ps --task_index=0 &
#python train_resnet.py --ps_hosts=node1:2222 --worker_hosts=localhost:2222,node3:2222,node4:2222,node5:2222,node6:2222,node7:2222 --data_dir=mnist --job_name=worker --task_index=0 --step=1000&
#python train_resnet.py --ps_hosts=node1:2222 --worker_hosts=node3:2222,localhost:2222,node4:2222 --data_dir=mnist --job_name=worker --task_index=1 --step=1000 &
python train_resnet.py --ps_hosts=node1:2222 --worker_hosts=localhost:2222 --data_dir=mnist --job_name=worker --task_index=0 --step=200 &
